//
class CF_base_rifleman: rhsusf_army_ocp_rifleman {
	author = AUTHOR;
	displayName = "[CF] Rifleman";
	uniform = "CR_base_uniform";
	backpack = "CR_backpack_Standard";
	faction = "CF_Whole_category";
	editorSubcategory = "CF_cheesecake_category";
	hiddenSelectionsTextures[] = {
		"CR_Attire/data/Uniforms/Base/CR_uniform_base.paa",
	};
	linkedItems[] = {"CR_opscore_mc_cover","CR_mbav_rifleman","rhsusaf_ANVPS_15","ItemRadio","ItemMap","ItemCompass","ItemWatch","ItemGPS"};
	respawnLinkedItems[] = {"CR_opscore_mc_cover","CR_mbav_rifleman","rhsusaf_ANVPS_15","ItemRadio","ItemMap","ItemCompass","ItemWatch","ItemGPS"};
};
class CF_rifleman_vladd: rhsusf_army_ocp_rifleman {//for testing
	author = AUTHOR;
	displayName = "[CF] Rifleman [vladd]";
	uniform = "CR_uniform_vladd";
	backpack = "CR_backpack_Standard";
	faction = "CF_Whole_category";
	editorSubcategory = "CF_highcomm_category";
	hiddenSelectionsTextures[] = {
		"CR_Attire/data/Uniforms/Highcomm/CR_uniform_Vladd.paa",
	};
	linkedItems[] = {"CR_opscore_mc_cover","CR_mbav_rifleman","rhsusaf_ANVPS_15","ItemRadio","ItemMap","ItemCompass","ItemWatch","ItemGPS"};
	respawnLinkedItems[] = {"CR_opscore_mc_cover","CR_mbav_rifleman","rhsusaf_ANVPS_15","ItemRadio","ItemMap","ItemCompass","ItemWatch","ItemGPS"};
};
class CF_rifleman_AJ: rhsusf_army_ocp_rifleman {//for testing
	author = AUTHOR;
	displayName = "[CF] Rifleman [AJ]";
	uniform = "CR_uniform_AJ";
	backpack = "CR_backpack_Standard";
	faction = "CF_Whole_category";
	editorSubcategory = "CF_highcomm_category";
	hiddenSelectionsTextures[] = {
		"CR_Attire/data/Uniforms/Highcomm/CR_uniform_AJ.paa",
	};
	linkedItems[] = {"CR_opscore_mc_cover","CR_mbav_rifleman","rhsusaf_ANVPS_15","ItemRadio","ItemMap","ItemCompass","ItemWatch","ItemGPS"};
	respawnLinkedItems[] = {"CR_opscore_mc_cover","CR_mbav_rifleman","rhsusaf_ANVPS_15","ItemRadio","ItemMap","ItemCompass","ItemWatch","ItemGPS"};
};
class CF_rifleman_datte: rhsusf_army_ocp_rifleman {//for testing
	author = AUTHOR;
	displayName = "[CF] Rifleman [Datte]";
	uniform = "CR_uniform_Datte";
	backpack = "CR_backpack_Standard";
	faction = "CF_Whole_category";
	editorSubcategory = "CF_highcomm_category";
	hiddenSelectionsTextures[] = {
		"CR_Attire/data/Uniforms/Highcomm/CR_uniform_Datte.paa",
	};
	linkedItems[] = {"CR_opscore_mc_cover","CR_mbav_rifleman","rhsusaf_ANVPS_15","ItemRadio","ItemMap","ItemCompass","ItemWatch","ItemGPS"};
	respawnLinkedItems[] = {"CR_opscore_mc_cover","CR_mbav_rifleman","rhsusaf_ANVPS_15","ItemRadio","ItemMap","ItemCompass","ItemWatch","ItemGPS"};
};
class CF_rifleman_marpat: rhsusf_army_ocp_rifleman {//for testing
	author = AUTHOR;
	displayName = "[CF] Rifleman [Marpat]";
	uniform = "CR_base_uniform_pc";
	backpack = "CR_backpack_Standard";
	faction = "CF_Whole_category";
	editorSubcategory = "CF_poundcake_category";
	hiddenSelectionsTextures[] = {
		"CR_Attire/data/Uniforms/Experimental/CR_uniform_pc_experimental.paa",
	};
	linkedItems[] = {"CR_opscore_mc_cover","CR_mbav_rifleman","rhsusaf_ANVPS_15","ItemRadio","ItemMap","ItemCompass","ItemWatch","ItemGPS"};
	respawnLinkedItems[] = {"CR_opscore_mc_cover","CR_mbav_rifleman","rhsusaf_ANVPS_15","ItemRadio","ItemMap","ItemCompass","ItemWatch","ItemGPS"};
};
//class CF_rifleman_marpat_g3: rhsusf_army_ocp_rifleman {//for testing
	author = AUTHOR;
	displayName = "[CF] Rifleman [Marpat G3]";
	uniform = "CR_base_uniform_pc";
	backpack = "CR_backpack_Standard";
	faction = "CF_Whole_category";
	editorSubcategory = "CF_poundcake_category";
	hiddenSelectionsTextures[] = {
		"CR_Attire/data/Uniforms/Poundcake/rhs_frog02_wd_co_ARES.paa",
	};
	linkedItems[] = {"CR_opscore_mc_cover","CR_mbav_rifleman","rhsusaf_ANVPS_15","ItemRadio","ItemMap","ItemCompass","ItemWatch","ItemGPS"};
	respawnLinkedItems[] = {"CR_opscore_mc_cover","CR_mbav_rifleman","rhsusaf_ANVPS_15","ItemRadio","ItemMap","ItemCompass","ItemWatch","ItemGPS"};
};
