//Berets
class CR_un_beret:rhsgref_un_beret {
	author = AUTHOR;
	displayName = "[CR] UN Beret";
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\clone_helmet_co.paa" Need textures
	//};
};
class CR_base_beret:rhsgref_un_beret {
	author = AUTHOR;
	displayName = "[CR] Cake Regiment Beret";
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\clone_helmet_co.paa" Need textures
	//};
};
class CR_cc_beret:rhsgref_un_beret {
	author = AUTHOR;
	displayName = "[CR] Cheesecake Beret";
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\clone_helmet_co.paa" Need textures
	//};
};
class CR_pc_beret:rhsgref_un_beret {
	author = AUTHOR;
	displayName = "[CR] Poundcake Beret";
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\clone_helmet_co.paa" Need textures
	//};
};
class CR_Cd_beret:rhsgref_un_beret {
	author = AUTHOR;
	displayName = "[CR] Caffeine Beret";
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\clone_helmet_co.paa" Need textures
	//};
};
class CR_rv_beret:rhsgref_un_beret {
	author = AUTHOR;
	displayName = "[CR] Red Velvet Beret";
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\clone_helmet_co.paa" Need textures
	//};
};

//Boonie hats
class CR_mc_boonie:rhs_Booniehat_ocp {
	author = AUTHOR;
	displayName = "[CR] Booniehat [MC]";
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\clone_helmet_co.paa" Need textures
	//};
};
class CR_marpat_boonie:rhs_booniehat2_marpatwd {
	author = AUTHOR;
	displayName = "[CR] Booniehat [Marpat]";
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\clone_helmet_co.paa" Need textures
	//};
};

//8 point hat
class CR_8point_mc:rhs_8point_marpatwd {
	author = AUTHOR;
	displayName = "[CR] 8 Point Hat [MC]";
	//needs texture
};
class CR_8point_marpat:rhs_8point_marpatwd {
	author = AUTHOR;
	displayName = "[CR] 8 Point Hat [Marpat]"
};

//Crew helmets
class CR_crew:rhsusf_cvc_green_heltmet {
	author = AUTHOR;
	displayName = "[CR] Crew Helmet";
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\clone_helmet_co.paa" Need textures
	//};
};
class CR_crew_ess:rhsusf_cvc_green_ess {
	author = AUTHOR;
	displayName = "[CR] Crew Helmet [ESS]";
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\clone_helmet_co.paa" Need textures
	//};
};

//Base Fast helmets 
class CR_opscore_mc_cover:rhsusf_opscore_mc_cover_pelt_nsw {
	author = AUTHOR;
	displayName = "[CR] Fast Opscore Covered [MC]";
	//needs textures
};
class CR_opscore_mc:rhsusf_opscore_mc_pelt_nsw {
	author = AUTHOR;
	displayName = "[CR] Fast Opscore [MC]";
	//needs texture
};
class CR_opscore_ut:rhsusf_opscore_ut_pelt_nsw {
	author = AUTHOR;
	displayName = "[CR] Fast Opscore [UT]";
};

//pilot helmets
class CR_heli_helmet:rhsusf_hgu56p_visor_black {
	author = AUTHOR;
	displayName = "[CR] Heli Pilot Helmet";
};

//Base Mich Helmet [experimental]
class CR_mich_experimental:rhsusf_mich_helmet_marpatwd_norotos_arc_headset {
	author = AUTHOR;
	displayName = "[CR] Experimental MICH"
};

//EXPERIMENTAL
class CR_parade_hat:H_ParadeDressCap_01_LDF_F {
	author = AUTHOR;
	displayName = "[CR] Parade Hat"
};

//add custom helmets soon


