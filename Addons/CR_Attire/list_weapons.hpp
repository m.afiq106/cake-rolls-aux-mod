// Helmets

"CR_un_beret",
"CR_base_beret",
"CR_cc_beret",
"CR_pc_beret",
"CR_Cd_beret",
"CR_rv_beret",
"CR_mc_boonie",
"CR_marpat_boonie",
"CR_8point_mc",
"CR_8point_marpat",
"CR_crew",
"CR_crew_ess",
"CR_opscore_mc_cover",
"CR_opscore_mc",
"CR_opscore_ut",
"CR_heli_helme",
"CR_mich_experimental",
"CR_parade_hat",

// Uniforms

"CR_base_uniform",
"CR_base_uniform_pastry",
"CR_base_uniform_pc",
"CR_uniform_vladd",
"CR_uniform_AJ",
"CR_uniform_Datte",

// Vests

"CR_Webbing",
"CR_mbav",
"CR_mbav_grenadier",
"CR_mbav_light",
"CR_mbav_mg",
"CR_mbav_medic",
"CR_mbav_rifleman",

// NVGs

//"TAG_MyNVG",