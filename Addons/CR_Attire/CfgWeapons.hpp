class CfgWeapons {
 // Base classes
 class U_I_CombatUniform;
 //Uniforms
    class U_B_FullGhillie_ish:U_I_CombatUniform {
	 class ItemInfo;
    };
    class U_I_E_Uniform_01_F:U_I_CombatUniform {// ldf uniforms [experimental]
	 class ItemInfo;
    };
    class U_I_E_Uniform_01_sweater_F:U_I_CombatUniform {// same as above
	 class ItemInfo;
    };
    class CSU15BP_Base_NG:U_I_CombatUniform {
	 class ItemInfo;
    };
    class rhsgref_uniform_specter:U_I_CombatUniform {
	 class ItemInfo;
    };
    class rhs_uniform_FROG01_wd:U_I_CombatUniform {
	 class ItemInfo;
    };
    class U_I_E_ParadeUniform_01_LDF_F:U_I_CombatUniform {
	 class ItemInfo;
    };

 //Helmets
 class rhsgref_un_beret;
 class rhs_Booniehat_ocp;
 class rhs_booniehat2_marpatwd;
 class rhsusf_cvc_green_heltmet;
 class rhsusf_cvc_green_ess;
 class rhsusf_opscore_mc_cover_pelt_nsw;
 class rhsusf_opscore_mc_pelt_nsw;
 class rhsusf_opscore_ut_pelt_nsw;
 class rhsusf_hgu56p_visor_black;
 class rhsusf_mich_helmet_marpatwd_norotos_arc_headset;
 class rhs_8point_marpatwd;
 class H_ParadeDressCap_01_LDF_F;

 //Vests
 class V_SmershVest_01_radio_F;
 class rhsusf_mbav;
 class rhsusf_mbav_grenadier;
 class rhsusf_mbav_light;
 class rhsusf_mbav_mg;
 class rhsusf_mbav_medic;
 class rhsusf_mbav_rifleman;
 class rhsusf_spc;
 class rhsusf_spc_corpsman;
 class rhsusf_spc_light;
 class rhsusf_spc_mg;
 class rhsusf_spc_marksman;
 class rhsusf_spc_Rifleman;
 class rhsusf_spc_teamleader;
 class rhsusf_spc_squadleader;

 //NVGs
 //might need to look into more.

 // Custom items
 #include "items_helmets.hpp"
 #include "items_uniforms.hpp"
 #include "items_vests.hpp"
 //#include "items_NVGs.hpp"
};