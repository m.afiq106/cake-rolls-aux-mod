/*
	Here are some notes to take before jumping into the sample.

	This sample mod is not intended to be a modding tutorial. It requires basic understanding of Arma 3 modding. For that I advise reading the Arma 3 wiki, specifically the links found below.

	The configurations and files are for rexture mods based on the Just Like The Simulations - The Great War mod.
	The "#inlude"ed "defines_..." files are the CfgWeaponsDefinitions.hpp and CfgVehiclesDefinitions.hpp files found in the Republic and Confederacy folders of the Google Drive just renamed.

	What this contains:
		-config.cpp						-> main configuration file
		
		-defines_CfgWeapons_GAR.hpp 	-> base class definitions file copied and renamed from the Republic folder of the JLTS modding assets Google Drive directory
		-defines_CfgWeapons_CIS.hpp 	-> base class definitions file copied and renamed from the Confederacy folder of the JLTS modding assets Google Drive directory
		-defines_CfgVehicles_GAR.hpp 	-> base class definitions file copied and renamed from the Republic folder of the JLTS modding assets Google Drive directory
		-defines_CfgVehicles_CIS.hpp 	-> base class definitions file copied and renamed from the Confederacy folder of the JLTS modding assets Google Drive directory
		
		-CfgWeapons.hpp					-> #include file of CfgWeapons
		-items_helmets.hpp				-> definitions of helmet items
		-items_uniforms.hpp				-> definitions of uniform items
		-items_vests.hpp				-> definitions of vest items
		-items_NVGs.hpp					-> definitions of NVG items
		-CfgVehicles.hpp				-> #include file of CfgVehicles
		-backpacks.hpp 					-> definitions of backpack items
		-units.hpp						-> definitions of units
		
		-data/ui/	-> folder for intentory item icons if you have any
		-data/ui/editorPreviews/	-> folder for editor preview imgages if you have any
		
	This set of sample files was put together by me, MrClock(#8163).
	If you run into issues with either the JLTS mods themselves or with making your own retexture mod that you can't resolve, feel free to join my discord https://discord.gg/KQSBDF3 where you can get help.


	USEFUL LINKS:

	https://community.bistudio.com/wiki/CfgPatches
	https://community.bistudio.com/wiki/Arma_3_Characters_And_Gear_Encoding_Guide
	https://community.bistudio.com/wiki/Arma_3_Assets
	https://community.bistudio.com/wiki/Eden_Editor:_Configuring_Asset_Previews

	PS:
	Use the built-in Config Viewer of Arma 3 that lets you look into the compiled master config.bin! It will save you from asking unneccessary questions.
	And don't forget, Google is your friend ;)
*/
#define AUTHOR You // macro to define the authors, replace "You" with whatever you like

class CfgPatches {
	class CR_Attire {
		author = AUTHOR;
		name = "Cake Rolls Attire"; // easier to understand name shown in the "Show Required Addons" option in 3DEN for instance
		requiredVersion = 2.0; // (optional) required Arma 3 verison
		requiredAddons[] = { // list of other CfgPatches classes that this addons depends on
			"rhsgref_c_troops",
			"rhsgref_main",
			"rhsgref_c_weapons",
			"rhs_c_troops",
			"rhsusf_main",
			"rhsusf_c_weapons",
			"rhsusf_c_troops",
			"rhs_main",
			"rhs_c_radio",
			"A3_Data_F",
			"A3_Weapons_F",
			"A3_Characters_F",
			"MoePilotCoveralls",
			"A3_Data_F_Enoch_Loadorder",
			//need 3cb factions,pilot gear,contact and base stuff
		};
		units[] = { // class names defined in CfgVehicles (everything, not only actual vehicles)
			#include "list_units.hpp"
		};
		weapons[] = { // class names defined in CfgWeapons (everything, not only actual weapons)
			#include "list_weapons.hpp"
		};
	
	
	};

	#include "CfgFactionClasses.hpp"

	#include "CfgEditorSubcategories.hpp"

	#include "CfgWeapons.hpp"

	#include "CfgVehicles.hpp"
};

