class CfgVehicles {
 // Base classes
 //Backpacks
 class UK3CB_B_Backpack_Pocket;
 class B_RadioBag_01_mtp_F;
 class rhs_rk_sht_30_olive;
 class rhs_rk_sht_30_olive_engineer_empty;
 class rhsusf_falconii_mc;
 class rhsusf_falconii_coy;
 class B_Carryall_mcamo;
 //class USP_TACTICAL_PACK_CCT5;

 //Units 
 class rhsusf_army_ocp_rifleman;

 // Custom backpacks and units
 #include "backpacks.hpp"
 #include "units.hpp"
};