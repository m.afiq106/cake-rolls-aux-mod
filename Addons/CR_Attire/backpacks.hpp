//standard backpacks
class CR_backpack_Standard: rhs_rk_sht_30_olive {
	author = AUTHOR;
	displayName = "[CR]Eagle Backpack";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\clone_backpack_co.paa"//need texture
	//};
};
class CR_backpack_medic: rhs_rk_sht_30_olive {
	author = AUTHOR;
	displayName = "[CR]Eagle Backpack (Medic)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};
class CR_backpack_EOD: rhs_rk_sht_30_olive {
	author = AUTHOR;
	displayName = "[CR]Eagle Backpack (EOD)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};
class CR_backpack_Explosives: rhs_rk_sht_30_olive {
	author = AUTHOR;
	displayName = "[CR]Eagle Backpack (Explosives)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};
class CR_backpack_Engineer: rhs_rk_sht_30_olive_engineer_empty {
	author = AUTHOR;
	displayName = "[CR]Eagle Backpack (Combat Engineer)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};

//poundcake backpacks
class CR_backpack_pc_Standard: rhs_rk_sht_30_olive {
	author = AUTHOR;
	displayName = "[CR]Eagle Backpack";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\clone_backpack_co.paa"//need texture
	//};
};
class CR_backpack_pc_medic: rhs_rk_sht_30_olive {
	author = AUTHOR;
	displayName = "[CR]Eagle Backpack (Medic)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};
class CR_backpack_pc_EOD: rhs_rk_sht_30_olive {
	author = AUTHOR;
	displayName = "[CR]Eagle Backpack (EOD)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};
class CR_backpack_pc_Explosives: rhs_rk_sht_30_olive {
	author = AUTHOR;
	displayName = "[CR]Eagle Backpack (Explosives)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};
class CR_backpack_pc_Engineer: rhs_rk_sht_30_olive_engineer_empty {
	author = AUTHOR;
	displayName = "[CR]Eagle Backpack (Combat Engineer)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};

//other backpacks
//class CR_backpack_tactical: UK3CB_B_Backpack_Pocket {
//	author = AUTHOR;
//	displayName = "[CR]Tactical Backpack";
//	scopeArsenal=2;
//	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
//};
class CR_backpack_rto: B_RadioBag_01_mtp_F{
	author = AUTHOR;
	displayName = "[CR]Radiopack (MC)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};
class CR_backpack_pc_rto: B_RadioBag_01_mtp_F{
	author = AUTHOR;
	displayName = "[CR]Radiopack (CBR)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};
class CR_falcon_mc: rhsusf_falconii_mc{
	author = AUTHOR;
	displayName = "[CR]Falcon Backpack (MC)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};
class CR_falcon_cbr: rhsusf_falconii_coy{
	author = AUTHOR;
	displayName = "[CR]Falcon Backpack (cbr)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};
class CR_carryall_mc: B_Carryall_mcamo {
	author = AUTHOR;
	displayName = "[CR]Carryall Backpack (MC)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};
class CR_carryall_cbr: B_Carryall_mcamo {
	author = AUTHOR;
	displayName = "[CR]Carryall Backpack (CBR)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};
//class CR_tactical_radio: USP_TACTICAL_PACK_CCT5 {
	author = AUTHOR;
	displayName = "[CR]Tactical Radiopack (MC)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};
//class CR_tactical_radio_cbr: USP_TACTICAL_PACK_CCT5 {
	author = AUTHOR;
	displayName = "[CR]Tactical Radiopack (CBR)";
	scopeArsenal=2;
	scope=2;
	//hiddenSelectionsTextures[] = {
	//	"JLTS_SAMPLE_1\data\b1_backpack_co.paa"//need texture
	//};
};
