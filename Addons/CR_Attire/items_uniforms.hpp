//Base uniforms
class CR_base_uniform:rhsgref_uniform_specter {
	author = AUTHOR;
	displayName = "[CR] Combat Uniform [1st Cake Regiment]";
	class ItemInfo: ItemInfo {
		uniformClass = "CF_base_rifleman"; // unit defined in CfgVehicles wearing this uniform
	};
};
class CR_base_uniform_pastry:rhsgref_uniform_specter {
	author = AUTHOR;
	displayName = "[CR] Combat Uniform [pastry]";
	//class ItemInfo: ItemInfo {
	//	uniformClass = "TAG_MyUnit_1"; // unit defined in CfgVehicles wearing this uniform
	//};
};
class CR_base_uniform_pc:rhsgref_uniform_specter {
	author = AUTHOR;
	displayName = "[CR] Combat Uniform [Poundcake] EXPERIMENTAL";
	class ItemInfo: ItemInfo {
		uniformClass = "CF_rifleman_marpat"; // unit defined in CfgVehicles wearing this uniform
	};
};

//High comm uniforms
class CR_uniform_vladd:rhsgref_uniform_specter {
	author = AUTHOR;
	displayName = "[CR] Combat Uniform [Vladd]";
	class ItemInfo: ItemInfo {
		uniformClass = "CF_rifleman_vladd"; // unit defined in CfgVehicles wearing this uniform
	};
};
class CR_uniform_AJ:rhsgref_uniform_specter {
	author = AUTHOR;
	displayName = "[CR] Combat Uniform [AJ]";
	class ItemInfo: ItemInfo {
		uniformClass = "CF_rifleman_AJ"; // unit defined in CfgVehicles wearing this uniform
	};
};
class CR_uniform_Datte:rhsgref_uniform_specter {
	author = AUTHOR;
	displayName = "[CR] Combat Uniform [Datte]";
	class ItemInfo: ItemInfo {
		uniformClass = "CF_rifleman_Datte"; // unit defined in CfgVehicles wearing this uniform
	};
};

//Cheesecake Uniforms
//class CR_uniform_Aiman: rhsgref_uniform_specter {
	//author = AUTHOR;
	//displayName = "[CR] Combat Uniform [Aiman]";
	//class ItemInfo: ItemInfo {
	//	uniformClass = "TAG_MyUnit_1"; // unit defined in CfgVehicles wearing this uniform
	//};
}//;
//class CR_uniform_Reaper: rhsgref_uniform_specter {
	//author = AUTHOR;
	//displayName = "[CR] Combat Uniform [Reaper]";
	//class ItemInfo: ItemInfo {
	//	uniformClass = "TAG_MyUnit_1"; // unit defined in CfgVehicles wearing this uniform
	//};
//};
//class CR_uniform_Twiggy: rhsgref_uniform_specter {
	//author = AUTHOR;
	//displayName = "[CR] Combat Uniform [Twiggy]";
	//class ItemInfo: ItemInfo {
	//	uniformClass = "TAG_MyUnit_1"; // unit defined in CfgVehicles wearing this uniform
	//};
//};

//Poundcake Uniforms
//class CR_marine_wd_base: rhs_uniform_FROG01_wd {
//	author = AUTHOR;
//	displayName = "[CR] Poundcake Uniform [Base]";
//	class ItemInfo: ItemInfo {
//		uniformClass = "CF_rifleman_marpat_g3"; // unit defined in CfgVehicles wearing this uniform
//	};
//};
