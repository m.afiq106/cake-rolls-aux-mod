class CfgEditorSubcategories {
	class CF_highcomm_category { // category to display the units in inside the faction
		displayName = "High Command";
	};
	class CF_cheesecake_category {
		displayName = "Cheesecake Company";
	};
	class CF_poundcake_category {
		displayName = "Poundcake Company";
	};
	class CF_Caffeine_category {
		displayName = "Caffeine Detachment";
	};
	class CF_Red_Velvet_category {
		displayName = "Red Velvet Squadron";
	};
};